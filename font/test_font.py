import pygame

pygame.init()
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()
done = False

font = pygame.font.SysFont("comicsansms", 72)

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            done = True

    screen.fill((255, 255, 255))

    for i, size in enumerate(range(10, 120, 10)):
        font = pygame.font.Font("DisposableDroidBB.ttf", size)
        text = font.render("Hello, World", True, (0, 128, 0))
        screen.blit(text,
            (320 - text.get_width() // 2, 20 + i*20 - text.get_height() // 2))

    pygame.display.flip()
    clock.tick(60)
