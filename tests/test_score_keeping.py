#!/usr/bin/env python

#
# Script containing unit tests for score logic
#
# Author: Niall Moran

import preclict.screens as screens
import pygame
from preclict.models import models
import preclict.utils as utils

def test_score_increments():
    screen = pygame.display.set_mode((1920, 1080), pygame.DOUBLEBUF)
    game = screens.GameScreen(screen, utils.ModelWrapper(models.load(None), prefix='plots/'), test_mode=False)
    game.update_score(-1.0)
    assert(not game._game_over())
    assert(not game._level_won())
    assert(not game._level_lost())

    for i in range(9): game.update_score(-1.0)
    assert(not game._game_over())
    assert(not game._level_won())
    assert(game._level_lost())




