#!/usr/bin/env python

#
# Script to setup and install preclict game
#

from setuptools import setup
import glob
import sys

print(sys.prefix)

setup(name='preclict',
      version='0.1',
      description='Simple python prediction game and framework',
      author='Niall Moran',
      author_email='niall@niallmoran.net',
      packages=['preclict', 'preclict.models'],
      scripts=['preclict/run_preclict.py', 'preclict/run_allvall.py', 'preclict/run_preclict_tests.py'],
      data_files=[('sounds', glob.glob('./sounds/*.wav')),
                  ('images', glob.glob('./images/*.png')),
                  ('text', glob.glob('./text/*.txt')),
                  ('font', glob.glob('./font/*.ttf'))],
      install_package_data=True,
      install_requires=['pygame==1.9.3', 'numpy==1.13.1',
                        'pandas==0.20.3', 'matplotlib==2.0.2',
                        'seaborn==0.8', 'scikit-learn==0.19.0',
                        'tqdm==4.15.0', 'pickleshare==0.7.4']
     )
