#!/usr/bin/env python

#
# Class for a sequence model whih will generate a repeating pattern of clicks.
#
# Author: Niall Moran

import numpy as np

class SequenceModel:
    """
    A model plays a repeating sequence
    """
    @staticmethod
    def usage():
        return '\tSequenceModel: \t\t\tModel which plays repeating sequence'

    def __init__(self, pattern='1', sep=''):
        self.ws = [] # keep for compatibility
        if sep == '':
            self.pattern =[int(x) for x in pattern]
        else:
            self.pattern = [int(x for x in pattern.split(sep))]
        self.repeat_count = 0 # count number of times current key has repeated
        self.pattern_idx = 0# keep track of position in pattern
        self.current = -1 # start with left
        self.x_old = 0 # initially set the former value to null value
        self.id = 'pattern_%s' % pattern

    def update_model(self, y):
        pass
        self.x_old = y
        repeat = self.pattern[self.pattern_idx % len(self.pattern)]
        self.repeat_count += 1
        if self.repeat_count == repeat:
            self.current *= -1
            self.pattern_idx += 1
            self.repeat_count = 0

    def predict(self):
        return self.current


    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old

    def reset(self, level): # we ignore level here
        self.x_old = 0 # initially set the former value to null value


