#!/usr/bin/env python

#
# Script to initialise models.
#
# Author: Niall Moran


import os
import re
import importlib
import json

path = os.path.dirname(__file__)

files = [re.sub("\.py$", "", y) for y in filter(lambda x: re.match('^[a-z].*\.py$', x), os.listdir(path))]

class Models(dict):

    def __init__(self):
        self.default = 'PerceptronWithMemoryModel'

    def list(self):
        print('Available models:')
        for model_name in self:
            print(self[model_name].usage() + ('(default)' if model_name == self.default else ''))

    def load(self, model_str):
        if model_str is None:
            model_name = self.default
            args = {}
        else:
            parts = model_str.split('.')
            if len(parts) > 1:
                args = json.loads(parts[1])
            else:
                args = {}
            model_name = parts[0]
        return self[(self.default if model_name is None else model_name)](**args)

models = Models()

for model_file in files:
    mod = importlib.import_module("preclict.models." + model_file)
    for obj in dir(mod):
        if obj.endswith("Model"):
            models[obj] = getattr(mod, obj)
