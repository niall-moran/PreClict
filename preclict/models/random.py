#!/usr/bin/env python

#
# Class for a model that selects button to click at random
#
# Author: Niall Moran

import numpy as np

class RandomModel:
    """
    A model which wraps a random number generator
    """
    @staticmethod
    def usage():
        return '\tRandomModel: \t\t\tRandom choice from random number generator'

    def __init__(self):
        self.ws = []
        self.x_old = 0 # initially set the former value to null value
        self.id = 'random'

    def update_model(self, y):
        pass
        self.x_old = y

    def predict(self):
        return -1 if np.random.randint(2) == 0 else 1

    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old

    def reset(self, level): # we ignore level here
        self.x_old = 0 # initially set the former value to null value


