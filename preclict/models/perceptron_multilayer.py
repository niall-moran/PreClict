#!/usr/bin/env python

#
# Multi-layer Perceptron model class
#
# Author: Niall Moran


import os, sys
import numpy as np
import logging
import argparse
from sklearn.neural_network import MLPClassifier


class PerceptronMultilayerModel:
    """
    Use the multi-layer perceptron model from sklearn
    """
    @staticmethod
    def usage():
        return '\tPerceptronMultilayer: \tMulti-layer perceptron model from sklearn {\'memory\':5}'

    def __init__(self, memory=5, hidden=tuple([4, 5])):
        self.hidden = hidden
        self.reset(memory)
        self.historical = 20

    def update_model(self, y):
        int_y = (0 if y == -1 else 1)
        self.mlp.fit(self.historical_X, self.historical_y)
        self.ws = np.hstack([x.ravel() for x in self.mlp.coefs_])

        self.x_old[:-1] = self.x_old[1:]
        self.x_old[-1] = int_y
        # move history along
        self.historical_X.append(self.x_old.copy())
        self.historical_y.append(int_y)
        if len(self.historical_X) > self.historical:
            self.historical_X.pop(0)
            self.historical_y.pop(0)


    def predict(self):
        pred = self.mlp.predict([self.x_old])[0]
        return (-1 if pred == 0 else 1)

    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old.copy()

    def reset(self, level):
        logging.debug('Resetting instance of perceptron model with memory=%d' % level)
        self.memory = level

        # calculate the number of weights
        self.n_weights = level*self.hidden[0]
        for i in range(1, len(self.hidden)):
            self.n_weights += self.hidden[i]*self.hidden[i-1]
        self.n_weights += self.hidden[-1]
        logging.debug("Multilayer with layers %s has %d weights." % (str(self.hidden), self.n_weights))
        self.x_old = np.zeros(self.memory)
        self.id = 'mlp_%d_%s' % (self.memory, ''.join([str(x) for x in self.hidden]))

        #self.mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=self.hidden)
        self.mlp = MLPClassifier(hidden_layer_sizes=self.hidden, max_iter=1000, learning_rate='adaptive', verbose=False)
        self.historical_X = [np.zeros(level)]
        self.historical_y = [0]
        self.mlp.fit(self.historical_X, self.historical_y)
        self.ws = np.hstack([x.ravel() for x in self.mlp.coefs_])
        assert(len(self.ws) == self.n_weights)



