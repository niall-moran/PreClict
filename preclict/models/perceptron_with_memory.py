#!/usr/bin/env python

#
# Class for perceptron model with more than one input
#
# Author: Niall Moran

import os, sys
import numpy as np
import logging
import argparse


class PerceptronWithMemoryModel:
    """
    Simple multiple input, one output perceptron which uses additional input
    for previous states.
    """
    @staticmethod
    def usage():
        return '\tPerceptronWithMemoryModel: \tMultiple input, single output perceptron model {\'memory\':5}'

    def __init__(self, memory=5):
        self.reset(memory)

    def update_model(self, y):
        exp_y = self._predict(self.x_old)
        self.ws += (y-exp_y)*np.hstack([[1.0], self.x_old])
        # move history along
        self.x_old[:-1] = self.x_old[1:]
        self.x_old[-1] = y

    def predict(self):
        return self._predict(self.x_old)

    def _predict(self, x):
        return (1 if (self.ws[0] + np.sum(self.ws[1:] * x)) > 0 else -1)

    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old.copy()

    def reset(self, level):
        logging.debug('Resetting instance of perceptron model with memory=%d' % level)
        self.memory = level
        self.ws = np.random.randn(self.memory+1) # one bias and one weight
        self.x_old = np.zeros(self.memory)
        self.id = 'p%d' % self.memory


