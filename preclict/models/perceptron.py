#!/usr/bin/env python

#
# Perceptron model class
#
# Author: Niall Moran



import numpy as np

class PerceptronModel:
    """
    Simple one input, one output perceptron
    """
    @staticmethod
    def usage():
        return '\tPerceptronModel: \t\tSingle input, single output perceptron model'

    def __init__(self):
        self.ws = np.random.randn(2) # one bias and one weight
        self.x_old = 0 # initially set the former value to null value
        self.id = 'p1'

    def update_model(self, y):
        exp_y = self._predict(self.x_old)
        self.ws += (y-exp_y)*np.array([1.0, self.x_old])
        self.x_old = y

    def predict(self):
        return self._predict(self.x_old)

    def _predict(self, x):
        return (1 if (self.ws[0] + self.ws[1]*x) > 0 else -1)

    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old

    def reset(self, level): # we ignore level here
        self.ws = np.random.randn(2) # one bias and one weight
        self.x_old = 0 # initially set the former value to null value


