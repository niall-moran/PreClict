#!/usr/bin/env python

#
# Utils classes
#
# Author: Niall Moran

import os, sys
import numpy as np
import logging
import pickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_agg as agg
import pygame
import time
import pandas as pd
from preclict.models import models
import random
from preclict.screens import *

base_path = os.path.dirname(os.path.dirname(__file__))

def sft(x):
    if x == 0:
        return -1
    else:
        return x

class PyManMain:
    def __init__(self, screen, model, test_mode=False, no_sound=False):
        self.screen = screen
        self.width, self.height = screen.get_width(), screen.get_height()
        self.model = model
        self.clock = pygame.time.Clock()
        self.state = 'splash'
        self.score_file = '%s/scores.csv' % base_path
        self.name, self.company, self.email, self.title = '', '', '', ''
        self.game_screen = GameScreen(self.screen, self.model, test_mode, no_sound=no_sound)
        self.demo_game_screen = GameScreen(self.screen, self.model, test_mode, model2=models.load('RandomModel'), no_sound=no_sound)

    def run_demo(self):
        HighScoresScreen(self.screen, self.score_file, delay=20).main_loop()
        self.demo_game_screen.reset()
        self.demo_game_screen.main_loop()

    def main_loop(self):
        while True:
            self.clock.tick(50)
            if self.state == 'splash':
                SplashScreen(self.screen, '', bg_image='%s/images/1 - You vs Machine.png' % base_path,
                             idle_time=60, idle_action=self.run_demo, score_file=self.score_file).main_loop()
                self.state = 'enter_details'
            if self.state == 'scores':
                high_scores = HighScoresScreen(self.screen, self.score_file,
                                               player={'name': self.name, 'email': self.email})
                high_scores.main_loop()
                self.state = 'splash'
            elif self.state == 'enter_details':
                self.name, self.email, self.company, self.title = DetailEntryScreen(self.screen).main_loop()
                if self.name is None:
                    self.state = 'splash'
                else:
                    self.state = 'game'
                    MessageScreen(self.screen, "Prepare for level 1",
                                bg_image='%s/images/14 - splash_blank.png' % base_path,
                                size='large').main_loop()
            elif self.state == 'game':
                self.game_screen.reset()
                self.write_score(self.game_screen.main_loop())
                self.state = 'scores'

    def write_score(self, score):
        try:
            previous_scores = pd.read_csv(self.score_file)
        except IOError as io:
            logging.error("Could not read score file %s, (%s)." % (self.score_file, str(io)))
            previous_scores = pd.DataFrame({'datetime': [], 'name': [], 'company': [], 'title': [], 'email': [], 'level': [], 'score': []})
        previous_scores = previous_scores.append({'name': self.name, 'score': score['overall_score'],
                                                  'email': self.email, 'level': score['level'],
                                                  'company': self.company, 'title': self.title,
                                                  'datetime' : time.time()},
                               ignore_index=True)
        previous_scores.sort_values(by='score', ascending=False, inplace=True)
        try:
            previous_scores.to_csv(self.score_file, index=False,
                                   columns=['datetime', 'name', 'email', 'company', 'title', 'level', 'score'])
            logging.info("%d scores written to score file." % (len(previous_scores)))
        except IOError as io:
            logging.error("Could not write score file %s, (%s)." % (self.score_file, str(io)))

    def exit(self, retval=0):
        pygame.display.quit()
        pygame.quit()
        sys.exit(retval)

class ModelWrapper:
    """
    Model wrapper class that takes care of a lot of the book keeping when running models.
    """

    def __init__(self, model, log_training=True, prefix='hist'):
        self.model = model

        # record history
        self.log_training = log_training
        self.prefix = prefix
        # lists to store inputs, outputs, weights, predictions and scores
        self.hX = []
        self.hy = []
        self.hws = []
        self.hps = []
        self.scores = [[0, 0]] # keep track of number of correct and incorrect predictions
        self.plot_history_steps = 5 # plot up to 5 steps back in time for history
        self.plot_model_steps = 20 # plot up to 20 steps back in time for model

    def update(self, x):
        if self.log_training:
            self.hX.append(self.model.last_x())
            self.hy.append(x)
        prediction = self.model.predict()
        if self.log_training:
            new_score = list(self.scores[-1])
            if prediction == x:
                new_score[0] += 1
            else:
                new_score[1] += 1
            self.scores.append(new_score)
            self.hps.append(prediction)
        self.model.update_model(x)
        if self.log_training:
            self.hws.append(self.model.weights())
        return prediction

    def plot_history(self, return_buf=False):
        fig = plt.figure(figsize=(6.4, 6.7), dpi=100)

        rel_hy = self.hy[-self.plot_history_steps:]
        rel_hps = self.hps[-self.plot_history_steps:]
        losses = [[x - len(rel_hy) + 1, rel_hy[x]] for x in range(len(rel_hy)) if rel_hy[x] == rel_hps[x]]
        wins = [[x - len(rel_hy) + 1, rel_hy[x]] for x in range(len(rel_hy)) if rel_hy[x] != rel_hps[x]]

        ax = plt.subplot(211)
        plt.plot([x[1] for x in losses], [x[0] for x in losses], ls='none', marker='o', color='#ec1e24', ms=10)
        plt.plot([x[1] for x in wins], [x[0] for x in wins], ls='none', marker='o', color='#8cc63f', ms=10)
        plt.xticks([-1.0, 1.0], ['left click', 'right click'])
        plt.ylim([-self.plot_history_steps, 1])
        plt.yticks([], [])
        plt.xlim([-2, 2])
        plt.ylabel('clicks')

        rel_hws = self.hws[-self.plot_model_steps:]
        times = np.arange(0.0, float(len(rel_hws)))
        ax = plt.subplot(212)
        plt.grid()
        if len(self.hws) > 0:
            for j in range(len(self.hws[0])):
                plt.plot(times + self.plot_model_steps - len(rel_hws) + 1, [x[j] for x in rel_hws], label='w_%d' % j, marker='.')
        plt.legend(loc='upper left')
        plt.title('Model')
        plt.xlabel('Time')
        plt.ylabel('Weights')
        tick_values = np.linspace(0.0, self.plot_model_steps, 5)
        tick_labels = ['%.0f' % (-(self.plot_model_steps - x)) for x in tick_values]
        plt.xlim((0, self.plot_model_steps))
        plt.xticks(tick_values, tick_labels)

        if not return_buf:
            plt.savefig('%s.png' % self.prefix)
            plt.close()
        else:
            canvas = agg.FigureCanvasAgg(fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            plt.close()
            return pygame.image.fromstring(raw_data, canvas.get_width_height(), "RGB")

    def accuracy(self):
        """
        Calculates the accuracy of the model. This is the number of times the model predicted the
        next click correctly.
        """
        model_wins = self.wins()
        attempts = len(self.hy)
        return (0 if attempts == 0 else model_wins/attempts)

    def inv_accuracy(self):
        """
        Calculates the accuracy of the model. This is the number of times the model predicted the
        next click correctly.
        """
        model_losses = self.losses()
        attempts = len(self.hy)
        return (0 if attempts == 0 else model_losses/attempts)

    def wins(self):
        """
        Calculates the number of wins
        """
        return len([x for i, x in enumerate(self.hy) if self.hy[i] == self.hps[i]])

    def losses(self):
        """
        Calculates the number of losses
        """
        return len([x for i, x in enumerate(self.hy) if self.hy[i] != self.hps[i]])

    def cleanup(self):
        if self.log_training:
            if self.prefix != '': self.plot_history()

    def reset(self, level):
        # set all history params to empty
        self.hX = []
        self.hy = []
        self.hws = []
        self.hps = []
        self.scores = [[0, 0]] # keep track of number of correct and incorrect predictions
        self.model.reset(level)

    def predict(self):
        return self.model.predict()

def load_msgs(filename):
    msgs = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            msgs.append(line)
    return msgs
