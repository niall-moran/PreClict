#!/usr/bin/env python

#
# Test how models get on against one another.
#
# Author: Niall Moran


import numpy as np
import argparse
import sys
from preclict.models import models
import preclict.utils as utils
import os


def run_all_left(m1, m2):
    model1 = models.load(m1)
    modelWrapper = utils.ModelWrapper(model1, prefix='plots/run_all_left_%s' % model1.id)
    for x in range(20): modelWrapper.update(-1)
    modelWrapper.cleanup()

def run_all_right(m1, m2):
    model1 = models.load(m1)
    modelWrapper = utils.ModelWrapper(model1, prefix='plots/run_all_right_%s' % model1.id)
    for x in range(20): modelWrapper.update(1)
    modelWrapper.cleanup()

def run_alternating(m1, m2):
    model1 = models.load(m1)
    modelWrapper = utils.ModelWrapper(model1, prefix='plots/run_alternating_%s' % model1.id)
    for x in range(20): modelWrapper.update(utils.sft(x % 2))
    modelWrapper.cleanup()

def run_random(m1, m2):
    model1 = models.load(m1)
    modelWrapper = utils.ModelWrapper(model1, prefix='plots/run_random_%s' % model1.id)
    for x in range(500): modelWrapper.update(utils.sft(np.random.randint(2)))
    modelWrapper.cleanup()

def run_comp_v_comp(m1, m2):
    model1 = models.load(m1)
    model2 = models.load(m2)
    modelWrapper1 = utils.ModelWrapper(model1, prefix='plots/run_comp1_%s' % model1.id)
    modelWrapper2 = utils.ModelWrapper(model2, prefix='plots/run_comp2_%s' % model2.id)
    model2_input = utils.sft(np.random.randint(2))
    for x in range(5000):
        model2_input = modelWrapper1.update(modelWrapper2.update(model2_input))
    modelWrapper1.cleanup()
    modelWrapper2.cleanup()

def gen_pattern(pattern, count):
    total = 100
    count = 0
    idx = 0
    while True:
        if count > total:
            break
        for j in range(pattern[idx % len(pattern)]):
            yield(utils.sft(idx%2))
            count += 1
        idx += 1

def evaluate_pattern(pattern, count, model):
    modelWrapper = utils.ModelWrapper(model, prefix='plots/pattern_%s_%s'
                                % (model.id, ''.join(map(lambda x: str(x), pattern))))
    for x in gen_pattern(pattern, count):
        modelWrapper.update(x)
    modelWrapper.cleanup()

def run_patterns(m1, m2):
    model1 = models.load(m1)
    evaluate_pattern([1,2,3], 100, model1)
    model1 = models.load(m1)
    evaluate_pattern([2], 100, model1)
    model1 = models.load(m1)
    evaluate_pattern([1,2], 100, model1)
    model1 = models.load(m1)
    evaluate_pattern([1,2,1,1,2], 100, model1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--list-tests', action='store_true', help='List available tests.')
    parser.add_argument('-l', '--list-models', action='store_true', help='List available models.')
    parser.add_argument('-m1', '--model1', type=str, default=None, help='First model to use.')
    parser.add_argument('-m2', '--model2', type=str, default=None, help='Second model to use.')
    parser.add_argument('tests', metavar='test_names', type=str, nargs='*', help='Tests to run.')
    opts = parser.parse_args(sys.argv[1:])

    tests = [ x for x in dir() if x.startswith('run')]

    if opts.list_tests:
        print('Available tests:')
        print('\t' + '\n\t'.join(tests))
        sys.exit(0)

    if opts.list_models:
        models.list()
        sys.exit(0)

    if not os.path.exists('plots/'):
        os.mkdir('plots')

    if len(opts.tests) == 0:
        for x in tests:
            print('Running ', x)
            locals()[x](opts.model1, opts.model2)
    else:
        for x in opts.tests:
            if x in tests:
                print('Running ', x)
                locals()[x](opts.model1, opts.model2)
            else:
                print('%s test not present' % x)
