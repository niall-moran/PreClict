#!/usr/bin/env python

#
# Script to play all models against all models and output accuracies in matrix form.
#
# Author: Niall Moran


import os, sys
import pygame
import numpy as np
import logging
import pickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import json
from preclict.models import models
import preclict.utils as utils
import numpy as np
import seaborn as sns
import tqdm
import itertools

def model_v_model(m1, m2, runs, steps, write_logs=False):
    """
    Play model1 vs model2. Model1 is trying to guess other click while
    model2 tries to guess which model1 will go for.
    """
    accuracies = [[], []]

    for run in range(runs):
        model1 = models.load(m1)
        model2 = models.load(m2)
        modelWrapper1 = utils.ModelWrapper(model1, prefix='')
        modelWrapper2 = utils.ModelWrapper(model2, prefix='')
        if write_logs: print('run, step, input, weights, prediction, update_weights')
        for x in range(steps):
            m2input = -modelWrapper1.predict()
            m2weights = modelWrapper2.model.ws.copy()
            val = modelWrapper2.update(m2input)
            m2weightsu = modelWrapper2.model.ws.copy()
            pred = modelWrapper2.predict()
            modelWrapper1.update(val)
            m1pred = -modelWrapper1.predict()
            if write_logs: print('%d\t%d\t%d\t%s\t%d\t%d\t%s\t%s' %
                                 (run, x, m2input, str(['%.2f' % x for x in m2weights]), pred, m1pred, str(['%.2f' % x for x in m2weightsu]), str(pred == m1pred)))
        modelWrapper2.cleanup()
        accuracies[0].append(modelWrapper1.inv_accuracy())
        accuracies[1].append(modelWrapper2.accuracy())
    return [np.mean(accuracies[0]), np.mean(accuracies[1])]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Utility that plays a set of models against all other models.')
    parser.add_argument('-r', '--runs', default=10, type=int, help='Run each match this number of times.')
    parser.add_argument('-s', '--steps', default=2000, type=int, help='Number of steps to run the models for.')
    parser.add_argument('-p', '--replot', default='', help='Replot from this accuracies text file.')
    opts = parser.parse_args(sys.argv[1:])

    logging.basicConfig(level=logging.INFO)

    model_names = [#'SequenceModel.{"pattern":"2", "sep":""}',
                   #'PerceptronModel',
                   'PerceptronWithMemoryModel.{"memory":1}',
                   'PerceptronWithMemoryModel.{"memory":2}',
                   'PerceptronWithMemoryModel.{"memory":3}',
                   #'PerceptronMultilayerModel'
                   #'PerceptronWithMemoryModel.{"memory":1}',
                   #'PerceptronWithMemoryModel.{"memory":2}']
    ]

    """
    model_names = ['RandomModel',
                   'SequenceModel.{"pattern":"1", "sep":""}',
                   'SequenceModel.{"pattern":"2", "sep":""}',
                   'SequenceModel.{"pattern":"112", "sep":""}',
                   'SequenceModel.{"pattern":"12121121212", "sep":""}',
                   'SequenceModel.{"pattern":"2313241123", "sep":""}',
                   'PerceptronWithMemoryModel.{"memory":1}',
                   'PerceptronWithMemoryModel.{"memory":2}',
                   'PerceptronWithMemoryModel.{"memory":3}',
                   'PerceptronWithMemoryModel.{"memory":4}',
                   'PerceptronMultilayerModel']
    """

    n_models = len(model_names)
    tick_labels = [models.load(x).id for x in model_names]
    tick_values = [x + 0.5 for x in range(len(model_names))]

    if opts.replot == '':
        accuracies1 = np.zeros((n_models, n_models))
        accuracies2 = np.zeros((n_models, n_models))
        for model_pair in tqdm.tqdm(list(itertools.product(enumerate(model_names), enumerate(model_names)))):
            i, model1 = model_pair[0]
            j, model2 = model_pair[1]
        #for i, model1 in enumerate(model_names):
        #    for j, model2 in enumerate(model_names):
            results = model_v_model(model1, model2, opts.runs, opts.steps)
            accuracies1[j, i] = results[0]
            accuracies2[i, j] = results[1]

        if not os.path.exists('plots'):
            os.mkdir('plots')
        np.savetxt('plots/accuracies1.txt', accuracies1)
        np.savetxt('plots/accuracies2.txt', accuracies2)

        ax = plt.subplot()
        sns.heatmap(accuracies1, annot=True, fmt='.2f', vmin=0.0, vmax=1.0)
        plt.title('Accuracies')
        ax.set_xticks(tick_values)
        ax.set_yticks(tick_values)
        ax.set_xticklabels(tick_labels, rotation=90)
        ax.set_yticklabels(tick_labels, rotation=0)
        plt.tight_layout()
        plt.savefig('plots/model_matrix.png')

        plt.close()
        ax = plt.subplot()
        sns.heatmap(accuracies2, annot=True, fmt='.2f', vmin=0.0, vmax=1.0)
        plt.title('Accuracies')
        ax.set_xticks(tick_values)
        ax.set_yticks(tick_values)
        ax.set_xticklabels(tick_labels, rotation=90)
        ax.set_yticklabels(tick_labels, rotation=0)
        plt.tight_layout()
        plt.savefig('plots/model_matrix_inv.png')

        overall_acc = np.sum(accuracies1, axis=1)
        print("Overall accuracies:")
        for i in np.argsort(overall_acc)[::-1]:
            print('%s: %.2g' % (model_names[i], overall_acc[i]/n_models))

    else:
        accuracies = np.loadtxt(opts.replot)
        if accuracies.shape[0] != n_models:
            logging.error("Must be same number of models as dimension of accuracies matrix.")
            sys.exit(1)
        ax = plt.subplot()
        sns.heatmap(accuracies, annot=True, fmt='.2f', vmin=0.0, vmax=1.0)
        plt.title('Accuracies')
        ax.set_xticks(tick_values)
        ax.set_yticks(tick_values)
        ax.set_xticklabels(tick_labels, rotation=90)
        ax.set_yticklabels(tick_labels, rotation=0)
        plt.tight_layout()
        plt.savefig('plots/model_matrix.png')

