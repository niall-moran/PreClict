#!/usr/bin/env python

#
# Screen classes
#
# Author: Niall Moran


import sys, os
import numpy as np
import logging
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_agg as agg
import pygame
import time
import pandas as pd

base_path = os.path.dirname(os.path.dirname(__file__))
print('Base path %s' % base_path)

def load_msgs(filename):
    msgs = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            msgs.append(line)
    return msgs

class GameScreen:

    def __init__(self, screen, model, test_mode, level_time_init_secs=0, extra_level_time=30,
                 model2=None, delay=1.0, level=1, no_sound=False):
        if not pygame.font: print("Warning, fonts disabled")
        if not pygame.mixer: print("Warning, sound disabled")
        pygame.init()
        self.game_text = InGameText()
        self.width, self.height = screen.get_width(), screen.get_height()
        self.screen = screen
        self.model = model
        self.clock = pygame.time.Clock()
        self.test_mode = test_mode
        self.show_plots = True
        self.automatic = model2 is not None
        self.model2 = model2
        self.delay = delay
        self.start_level = level
        self.no_sound = no_sound

        # game play parameters
        self.end_score = 10
        self.max_clicks = 100
        self.level_time_init_secs = level_time_init_secs
        self.extra_level_time = extra_level_time

        # style and colours
        self.bg_colour = (0, 0, 0, 255)
        self.font_colour = (255, 255, 255)
        self.hfont_colour = (255, 0, 0)

        self.green_bg = pygame.image.load('%s/images/3 - Main Screen Green.png' % base_path)
        self.red_bg = pygame.image.load('%s/images/3 - Main Screen Red.png' % base_path)

        self.red_thumb = pygame.image.load('%s/images/thumb_red.png' % base_path)
        self.green_thumb = pygame.image.load('%s/images/thumb_green.png' % base_path)

        self.good_sound = pygame.mixer.Sound('%s/sounds/sfx_coin_single1.wav' % base_path)
        self.bad_sound = pygame.mixer.Sound('%s/sounds/sfx_sounds_damage3.wav' % base_path)

        self.levelup_sound = pygame.mixer.Sound('%s/sounds/sfx_sounds_fanfare3.wav' % base_path)
        self.death_sound = pygame.mixer.Sound('%s/sounds/sfx_deathscream_robot3.wav' % base_path)

        self.winning_messages = load_msgs('%s/text/winning.txt' % base_path)
        self.losing_messages =  load_msgs('%s/text/losing.txt' % base_path)

        # change the plotting style of matplotlib
        plt.rcParams['figure.facecolor'] = u'k'
        plt.rcParams['figure.edgecolor'] = u'w'
        plt.rcParams['axes.facecolor'] = u'k'
        plt.rcParams['axes.edgecolor'] = u'w'
        plt.rcParams['text.color'] = u'w'
        plt.rcParams['xtick.color'] = u'w'
        plt.rcParams['ytick.color'] = u'w'

        self.reset()

    def process_mouse_button(self, button):
        if button == 1:
            self.process_click(-1)
            self.updated = True
        elif button == 3:
            self.process_click(1)
            self.updated = True

    def update_score(self, change):
        self.score += change
        self.max_score = max(self.score, self.max_score)

    def _calc_level_score(self):
        """
        The level score is calculated from highest lead attained times level plus any time remaining.
        """
        return self.max_score * self.level + self.level_time_left_secs

    def _current_score(self):
        return self.overall_score + self.max_score * self.level

    def _out_of_time(self):
        return self.level_time_left_secs <= 0.0

    def _game_over(self):
        return self._level_lost() and self.lives <= 0

    def _level_lost(self):
        return self.score <= -self.end_score or self._out_of_time()

    def _level_won(self):
        return self.score >= self.end_score

    def level_points(self, level):
        return (-min(0.5 + (level-1)*0.1, 1.0), 1.0)

    def calc_level_time(self, level):
        return self.level_time_init_secs + level * self.extra_level_time

    def _win_msg(self):
        if len(self.winning_messages) > 0:
            return self.winning_messages[np.random.randint(0, len(self.winning_messages))]
        else:
            return ''

    def _lose_msg(self):
        if len(self.losing_messages) > 0:
            return self.losing_messages[np.random.randint(0, len(self.losing_messages))]
        else:
            return ''

    def _update_gamestate(self):
        if self._level_won():
            self.overall_score += self._calc_level_score()
            self.level += 1
            self.reset_level()
            if not self.no_sound: self.levelup_sound.play()
            MessageScreen(self.screen,
                          self._win_msg() + " You are onto level %d!" % self.level,
                          bg_image='%s/images/11 - Level Up.png' % base_path, offset = (0, 150),
                          size='medium', delay=(1 if self.automatic else -1)).main_loop()
        elif self._level_lost():
            self.overall_score += self._calc_level_score()
            self.lives -= 1
            if self.lives > 0:
                if not self.no_sound: self.death_sound.play()
                MessageScreen(self.screen,
                              self._lose_msg() + " You failed to pass level %d" % self.level,
                              bg_image='%s/images/13 - Try Again.png' % base_path, offset = (0, 150),
                              size='medium', delay=(1 if self.automatic else -1)).main_loop()
                self.reset_level()

    def process_click(self, x):
        logging.info("%d click" % x)
        self.predicted_value = self.model.update(x)
        self.click_count += 1
        if self.predicted_value == x:
            logging.info("You lose, %d == %d" % (self.predicted_value, x))
            if not self.no_sound: self.bad_sound.play()
            self.last_point = 'lose_%s' % ('left' if x == -1 else 'right')
            self.update_score(self.point_loss)
        else:
            logging.info("You win, %d == %d" % (self.predicted_value, x))
            if not self.no_sound: self.good_sound.play()
            self.last_point = 'win_%s' % ('left' if x == -1 else 'right')
            self.update_score(self.point_win)
        logging.info("Score: %d" % self.score)
        self.message = 'Your score is: %d' % (self.score)
        self.previousClick = x

    def main_loop(self):
        # only used in automatic (demo) mode. Used to keep track of last time an automatic move was made
        last_automatic_update = None
        while True:
            now = time.time()
            # limit game to 50 fps
            self.clock.tick(50)
            if self.previousClick == 0:
                # if level starting, set start time
                self.level_start_secs = time.time()
            else:
                # otherwise clear pop up messages and update time left
                self.pop_up_msgs = []
                self.level_time_left_secs = self.level_time_secs - (time.time() - self.level_start_secs)
            # will only replot history if something has changed
            self.updated = False
            if self.automatic:
                if last_automatic_update is None or (now - last_automatic_update)  > self.delay:
                    # if in automatic mode and ready to make another move, use second model to predict the next move and redraw
                    model_prediction = self.model2.predict()
                    mouse_click_code = (1 if model_prediction == -1 else 3) # 1 is left click and 3 is right
                    self.process_mouse_button(mouse_click_code)
                    self.model2.update_model(self.predicted_value)
                    last_automatic_update = now
            for event in pygame.event.get():
                # process events
                if event.type == pygame.QUIT:
                    self.model.cleanup()
                    self.exit()
                if self.automatic:
                    # if in automatic mode and a key or button is pressed, we return
                    if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                        return {'overall_score': self.overall_score, 'level': self.level}
                else:
                    if event.type == pygame.KEYDOWN:
                        # process key press
                        if event.key == pygame.K_q or pygame.K_ESCAPE:
                            # q or escape returns
                            return {'overall_score': self.overall_score, 'level': self.level}
                        elif event.key == pygame.K_p:
                            # p toggles display of plot
                            logging.info("Toggling plot display")
                            self.show_plots = not self.show_plots
                        elif event.key == pygame.K_f:
                            # f toggles fullscreen
                            logging.info("Toggling fullscreen")
                            pygame.display.toggle_fullscreen()
                            self.cleanup()
                    elif event.type == pygame.MOUSEBUTTONDOWN:
                        # process mouse press events
                        if not self.automatic: self.process_mouse_button(event.button)
            if self._game_over():
                # if the game is over, display message and return score
                if not self.no_sound: self.death_sound.play()
                MessageScreen(self.screen, "",
                                bg_image='%s/images/10 - Game Over Red.png' % base_path, delay=(1 if self.automatic else -1)).main_loop()
                return {'overall_score': self.overall_score, 'level': self.level}
            self._update_gamestate()
            self.do_update()

    def do_update(self):
        # clear screen
        self.screen.fill(self.bg_colour)
        if self.score < 0:
            self.screen.blit(self.red_bg, (0, 0))
        else:
            self.screen.blit(self.green_bg, (0, 0))

        header_height = 40
        lives_pos = (1850, 120)
        level_pos = (1850, 220)
        score_pos = (1875, 45)
        lead_pos = (80, 1000)
        time_pos = (1840, 1000)
        if self.show_plots:
            left_pos = (320, 620)
            right_pos = (960, 620)
        else:
            left_pos = (480, 620)
            right_pos = (1440, 620)

        # draw the current message we have
        self.game_text.draw_centred(self.screen, "%d" % (self.lives),
                                    self.font_colour, lives_pos, 'large')

        self.game_text.draw_centred(self.screen, "%d" % (self.level),
                                    self.font_colour, level_pos, 'large')

        self.game_text.draw_right(self.screen, "Score: %.1f" % self._current_score(),
                                  self.font_colour, score_pos, 'medium')

        self.game_text.draw_left(self.screen, "Lead (max): %.1f (%.1f)" % (self.score, self.max_score),
                                 self.font_colour, lead_pos, 'medium')

        self.game_text.draw_centred(self.screen, "Accuracy: %.3g" % (self.model.accuracy()),
                                 self.font_colour, (self.width//2, 1000), 'medium')

        self.game_text.draw_right(self.screen, "Time: %.1f" % self.level_time_left_secs,
                                 self.font_colour, time_pos, 'medium')

        if self.last_point is not None:
            if self.last_point.startswith('lose'):
                img = self.red_thumb
            else:
                img = self.green_thumb
            if self.last_point.endswith('left'):
                pos = left_pos
            else:
                pos = right_pos
            self.screen.blit(img, (pos[0] - img.get_width()//2, pos[1] - img.get_height()//2))

        if self.show_plots:
            self.draw_history()

        if len(self.pop_up_msgs) > 0:
            self.game_text.pop_up_box(self.screen, self.pop_up_msgs, self.font_colour, (self.width//2, self.height//2))

        # render updates
        pygame.display.update()

    def cleanup(self):
        self.model.cleanup()

    def reset(self):
        self.overall_score = 0
        if not self.test_mode:
            self.level = self.start_level
            self.lives = 3
        else:
            self.level = self.start_level
            self.lives = 1

        self.reset_level()

    def reset_level(self):
        self.score = 0
        self.max_score = 0
        self.click_count = 0
        self.model.reset(self.level)
        self.previousClick = 0
        self.predicted_value = None
        self.message = 'Get a score of %d within %d clicks' % (self.end_score, self.max_clicks)
        self.updated = True
        self.plot = None
        self.last_point = None
        self.level_time_secs = self.calc_level_time(self.level)
        self.level_time_left_secs = self.level_time_secs
        self.level_start_secs = time.time()
        self.point_loss, self.point_win = self.level_points(self.level)
        self.pop_up_msgs = (self.long_instructions() if self.level == 1 else self.compact_instructions())

    def long_instructions(self):
        return ['Try to be as unpredictable as possible!',
                '',
                'Lose %.2f points for clicking the expected button and get' % (-self.point_loss),
                '%.2f for each unexpected click. Get to %d points in %d' % (self.point_win, self.end_score, self.level_time_left_secs),
                'seconds to advance to the next level.',
                '',
                'Level score = max_lead * level + remaining_time']

    def compact_instructions(self):
        return ['Lose %.2f points for clicking the expected button and get' % (-self.point_loss),
                '%.2f for each unexpected click. Get to %d points in %d' % (self.point_win, self.end_score, self.level_time_left_secs),
                'seconds to advance to the next level.']

    def draw_history(self):
        if self.updated:
            s = time.time()
            self.plot = self.model.plot_history(return_buf=True)
            e =  time.time()
            logging.debug("Took %f to draw image." % (e-s))
        if self.plot is not None:
            self.screen.blit(self.plot, (1280, 300))

    def exit(self, retval=0):
        pygame.display.quit()
        pygame.quit()
        sys.exit(retval)


class HighScoresScreen:

    def __init__(self, screen, scores_file, player=None, delay=-1):
        self.screen = screen
        self.width, self.height = screen.get_width(), screen.get_height()
        try:
            self.scores = pd.read_csv(scores_file)
        except IOError as io:
            logging.error("Problem loading scores file %s." % scores_file)
            self.scores = pd.DataFrame({'datetime': [], 'name': [], 'company': [], 'title': [], 'email': [], 'level': [], 'score': []})
        self.screen_index = 0
        self.number_per_screen = 5
        self.max_screens = int(np.floor(len(self.scores) / self.number_per_screen))
        self.clock = pygame.time.Clock()
        self.game_text = InGameText()
        self.font_colour = (255, 255, 255)
        self.hfont_colour = (255, 0, 0)
        self.bg = pygame.image.load('%s/images/12 - Scoreboard.png' % base_path)
        self.player = player
        self.player_index = None
        self.delay = delay
        self.update_scores()

    def update_scores(self):
        self.scores = self.scores.groupby(['name', 'email']).max()[['score']].reset_index()
        self.scores = self.scores.sort_values(by='score', ascending=False).reset_index()
        del self.scores['index']
        if self.player_index is None and self.player is not None:
            me_indices = np.where((self.scores['name'] == self.player['name']) &
                                (self.scores['email'] == self.player['email']))[0]
            if len(me_indices) > 0:
                logging.info('Found player %s at index %d' % (self.player['name'], me_indices[0]))
                self.player_index = me_indices[0]
                self.screen_index = self.player_index // self.number_per_screen

        score_start = min(len(self.scores), self.screen_index * self.number_per_screen)
        self.display_scores = self.scores.loc[slice(score_start, score_start + self.number_per_screen - 1), :]
        logging.info("Updated order of %d scores" % (len(self.display_scores)))

    def next_screen(self):
        self.screen_index = min(self.screen_index + 1, self.max_screens)
        self.update_scores()

    def previous_screen(self):
        self.screen_index = max(self.screen_index - 1, 0)
        self.update_scores()

    def main_loop(self):
        start = time.time()
        while True:
            if self.delay != -1 and time.time() - start > self.delay:
                return
            self.clock.tick(50)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logging.debug("Quiting.")
                    self.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_DOWN:
                        self.next_screen()
                    elif event.key == pygame.K_UP:
                        self.previous_screen()
                    elif event.key == pygame.K_q:
                        logging.debug("Quiting from high scores screen")
                        self.exit()
                    else:
                        logging.debug("Returning from high scrores screen.")
                        return
                #elif event.type == pygame.MOUSEBUTTONDOWN:
                #    return
            self.do_update()

    def do_update(self):
        # clear screen
        self.screen.fill((0,0,0,0))
        self.screen.blit(self.bg, (0, 0))
        header_height = 300
        name_offset = self.width // 4
        number_offset = name_offset - 100
        score_offset = 3 * self.width // 4
        score_height = 90
        centre_offset = self.width // 2
        padding = 25

        offset = self.game_text.draw_centred(self.screen, "HI SCORES", self.font_colour, (centre_offset, header_height), size='large')

        title_columns_height =  offset + 3 * padding
        self.game_text.draw_left(self.screen, "#", self.font_colour,
                                 (number_offset, title_columns_height), size='medium')

        self.game_text.draw_left(self.screen, "Name", self.font_colour,
                                 (name_offset, title_columns_height), size='medium')

        offset = self.game_text.draw_left(self.screen, "Score", self.font_colour,
                                 (score_offset, title_columns_height), size='medium')

        offset += padding

        for score_row in self.display_scores.iterrows():
            i = score_row[0] + 1
            name = score_row[1]['name']
            score = score_row[1]['score']
            font_colour = (self.hfont_colour if self.player_index == score_row[0] else self.font_colour)

            self.game_text.draw_left(self.screen, str(i) + ':', font_colour,
                                     (number_offset, offset + padding), size='medium')

            self.game_text.draw_left(self.screen, str(name), font_colour,
                                     (name_offset, offset + padding), size='medium')

            offset = self.game_text.draw_left(self.screen, '%.3f' % score, font_colour,
                                     (score_offset, offset + padding), size='medium')

        # press any key
        self.game_text.draw_centred(self.screen, "Press any key", self.font_colour,
                                    (centre_offset, self.height - 5 * padding))

        # render updates
        pygame.display.update()

    def exit(self, retval=0):
        pygame.display.quit()
        pygame.quit()
        sys.exit(retval)


class DetailEntryScreen:

    def __init__(self, screen):
        self.screen = screen
        self.clock = pygame.time.Clock()
        self.name = ""
        self.email = ""
        self.company = ""
        self.title = ""
        self.focus = "name"
        self.bg = pygame.image.load('%s/images/2 - Enter Details Screen.png' % base_path)
        self.game_text = InGameText()
        self.width, self.height = self.screen.get_width(), self.screen.get_height()
        self.font_colour = (255, 255, 255)

    def main_loop(self):
        while True:
            self.clock.tick(50)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logging.debug("Quiting.")
                    self.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        if self.complete_entry():
                            return self.name, self.email, self.company, self.title
                    elif event.key == pygame.K_BACKSPACE:
                        self.backspace()
                    elif event.key == pygame.K_ESCAPE:
                        if self.focus == 'name':
                            return None, None, None, None
                        elif self.focus == 'email':
                            self.email = ''
                            self.focus = 'name'
                        elif self.focus == 'company':
                            self.company = ''
                            self.focus = 'name'
                        elif self.focus == 'title':
                            self.title = ''
                            self.focus = 'company'
                    else:
                        self.append_key(event.key, event.unicode)
            self.do_update()

    def do_update(self):
        # clear screen
        self.screen.fill((0,0,0,0))
        self.screen.blit(self.bg, (0, 0))
        header_height = 50
        name_offset = self.width // 4
        padding = 40

        offset = self.game_text.draw_centred(self.screen, "ENTER DETAILS", self.font_colour,
                                             (self.width//2, header_height), 'large')

        # name field
        self.game_text.draw_right(self.screen, "Name:", self.font_colour,
                                  (name_offset, offset+padding), 'medium')
        offset = self.game_text.draw_left(self.screen, self.name + ('_' if self.focus == "name" else ''),
                                          self.font_colour, (name_offset + padding, offset+padding), 'medium')

        # email field
        self.game_text.draw_right(self.screen, "Email:", self.font_colour,
                                  (name_offset, offset+padding), 'medium')
        offset = self.game_text.draw_left(self.screen, self.email + ('_' if self.focus == "email" else ''),
                                          self.font_colour, (name_offset + padding, offset+padding), 'medium')

        # company field
        self.game_text.draw_right(self.screen, "Company:", self.font_colour,
                                  (name_offset, offset+padding), 'medium')
        offset = self.game_text.draw_left(self.screen, self.company + ('_' if self.focus == "company" else ''),
                                          self.font_colour, (name_offset + padding, offset+padding), 'medium')

        # title field
        self.game_text.draw_right(self.screen, "Title:", self.font_colour,
                                  (name_offset, offset+padding), 'medium')
        offset = self.game_text.draw_left(self.screen, self.title + ('_' if self.focus == "title" else ''),
                                          self.font_colour, (name_offset + padding, offset+padding), 'medium')

        self.game_text.draw_centred(self.screen, "ESC to go back", self.font_colour,
                                    (self.width//2, offset + padding), 'medium')

        # render updates
        pygame.display.update()

    def backspace(self):
        """
        When backspace is pressed, remove a character from field in focus.
        """
        if self.focus == "name":
            if len(self.name) > 0: self.name = self.name[:-1]
        elif self.focus == "email":
            if len(self.email) > 0: self.email = self.email[:-1]
        elif self.focus == "company":
            if len(self.company) > 0: self.company = self.company[:-1]
        elif self.focus == "title":
            if len(self.title) > 0: self.title = self.title[:-1]

    def check_email(self, email):
        """
        Do very simple check that mail address if valid
        """
        if len(email) == 0:
            logging.debug('Invalid email')
            return False
        if email.find('@') == -1 or email.find('.') == -1:
            logging.debug('Invalid email')
            return False
        else:
            return True

    def complete_entry(self):
        if self.focus == "name" and len(self.name) > 0:
            self.focus = "email"
            return False
        elif self.focus == "email" and self.check_email(self.email):
            self.focus = "company"
            return False
        elif self.focus == "company" and len(self.company) > 0:
            self.focus = "title"
            return False
        elif self.focus == "title" and len(self.title) > 0:
            return True
        else:
            return False

    def append_key(self, key, unicode):
        logging.debug("Got key %s -> %s as unicode %s" % (key, chr(int(key)), unicode))
        if int(key) <= ord('z'):
            if self.focus == "name":
                self.name = self.name + unicode
            elif self.focus == "email":
                self.email = self.email + unicode
            elif self.focus == "company":
                self.company = self.company + unicode
            elif self.focus == "title":
                self.title = self.title + unicode

    def exit(self, retval=0):
        pygame.display.quit()
        pygame.quit()
        sys.exit(retval)


class MessageScreen:

    def __init__(self, screen, msg, bg_image='', fill=(0,0,0,0), font_colour=(255, 255, 255), size='medium',
                 offset=(0, 0), delay=-1):
        self.screen = screen
        self.clock = pygame.time.Clock()
        self.msg = msg
        self.game_text = InGameText()
        self.fill = fill
        self.font_colour = font_colour
        if bg_image != '':
            self.bg = pygame.image.load(bg_image)
        else:
            self.bg = None
        self.size = size
        self.offset = offset
        self.delay = delay

    def main_loop(self):
        start = time.time()
        while True:
            self.clock.tick(50)
            if self.delay != -1 and time.time() - start > self.delay:
                return
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logging.debug("Quiting.")
                    self.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        self.exit()
                    return
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    return
            self.do_update()

    def do_update(self):
        # clear screen
        self.screen.fill(self.fill)
        if self.bg is not None:
            self.screen.blit(self.bg, (0, 0))

        if self.msg != '':
            self.game_text.draw_centred(self.screen, self.msg,
                                        self.font_colour,
                                        (self.screen.get_width()//2 + self.offset[0],
                                         self.screen.get_height()//2 + self.offset[1]),
                                        self.size)

        # render updates
        pygame.display.update()

    def exit(self, retval=0):
        pygame.display.quit()
        pygame.quit()
        sys.exit(retval)

class SplashScreen(MessageScreen):

    def __init__(self, screen, msg, bg_image='', fill=(0,0,0,0), font_colour=(255, 255, 255), size='medium',
                 offset=(0, 0), delay=-1, score_file='', idle_time=-1, idle_action=None):

        super(SplashScreen, self).__init__(screen, msg, bg_image, fill, font_colour, size, offset, delay)
        self.idle_time = idle_time
        self.idle_action = idle_action
        self.score_file = score_file

    def main_loop(self):
        start = time.time()
        start_idle = time.time()
        while True:
            self.clock.tick(50)
            if self.delay != -1 and time.time() - start > self.delay:
                return
            if self.idle_time != -1 and time.time() - start_idle > self.idle_time:
                self.idle_action()
                start_idle = time.time()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logging.debug("Quiting.")
                    self.exit()
                elif event.type == pygame.KEYDOWN:
                    start_idle = time.time()
                    if event.key == pygame.K_q:
                        self.exit()
                    elif event.key == pygame.K_s or event.key == pygame.K_h:
                        HighScoresScreen(self.screen, self.score_file).main_loop()
                    else:
                        return
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    start_idle = time.time()
                    return
            self.do_update()

class InGameText:
    """
    Class to deal with writing fonts in different styles throughout the game
    """

    def __init__(self):
        # load the different fonts that will be used throughout
        pygame.font.init()
        self.fonts = {}
        self.fonts['small'] = pygame.font.Font("%s/font/DisposableDroidBB.ttf" % base_path, 40)
        self.fonts['medium'] = pygame.font.Font("%s/font/DisposableDroidBB.ttf" % base_path, 60)
        self.fonts['large'] = pygame.font.Font("%s/font/DisposableDroidBB.ttf" % base_path, 100)

    def draw_centred(self, screen, msg, colour, pos, size='small'):
        """
        Draw the given message centreed at the given position
        """
        text = self.fonts[size].render(msg, True, colour) # true is antialiasing
        screen.blit(text, (pos[0] - text.get_width()//2, pos[1] - text.get_height() // 2))
        return pos[1] + text.get_height() // 2

    def draw_left(self, screen, msg, colour, pos, size='small'):
        """
        Draw the given message centreed at the given position
        """
        text = self.fonts[size].render(msg, True, colour) # true is antialiasing
        screen.blit(text, (pos[0], pos[1] - text.get_height() // 2))
        return pos[1] + text.get_height() // 2

    def draw_right(self, screen, msg, colour, pos, size='small'):
        """
        Draw the given message centreed at the given position
        """
        text = self.fonts[size].render(msg, True, colour) # true is antialiasing
        screen.blit(text, (pos[0] - text.get_width(), pos[1] - text.get_height() // 2))
        return pos[1] + text.get_height() // 2


    def pop_up_box(self, screen, msgs, colour, pos, size='medium', padding=10, border=2, centred=True):
        """
        Overlay a box and draw message inside it.
        """
        texts = []
        dims = []
        for msg in msgs:
            text = self.fonts[size].render(msg, True, colour)
            dims.append((text.get_width(), text.get_height()))
            texts.append(text)

        max_width = np.max([x[0] for x in dims])
        text_height = dims[0][1]

        height = text_height * len(msgs) + (len(msgs) + 1) * padding
        width = max_width + 2 * padding

        # draw box and border
        pygame.draw.rect(screen, (0, 0, 0), (pos[0] - width//2, pos[1] - height//2, width, height), 0)

        # draw box and border
        pygame.draw.rect(screen, colour, (pos[0] - width//2, pos[1] - height//2, width, height), border)

        for i, text in enumerate(texts):
            if centred:
                line_width = dims[i][0]
                screen.blit(text, (pos[0] - line_width//2, pos[1] - height//2 + (i+1)*padding + i*text_height))
            else:
                screen.blit(text, (pos[0] - width//2, pos[1] - height//2 + (i+1)*padding + i*text_height))
