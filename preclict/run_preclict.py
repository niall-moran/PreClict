#!/usr/bin/env python

#
# Script to start the game.
#
# Author: Niall Moran
#

import os, sys
import pygame
import numpy as np
import logging
import pickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import json
from preclict.models import models
import preclict.utils as utils


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--list', help='List the models that one can use', action='store_true')
    parser.add_argument('model_str', nargs='?', help='Model to use with args appended after a full stop.')
    parser.add_argument('-id', default='hist', help='Identifier to use for output files (default=hist).')
    parser.add_argument('-t', '--test-mode', action='store_true', help='Run in test mode without asking for user details.')
    parser.add_argument('-w', '--windowed', action='store_true', help='Start windowed')
    parser.add_argument('--no-sound', action='store_true')
    parser.add_argument('--scores', action='store_true')
    parser.add_argument('--gameonly', action='store_true')
    parser.add_argument('--gameonly-level', default=1, type=int)
    parser.add_argument('--gameonly-model', default='', type=str)
    parser.add_argument('--log-level', default='INFO', type=str)
    opts = parser.parse_args(sys.argv[1:])

    # set logging mode
    logging.basicConfig(level=logging.DEBUG)

    if opts.list:
        # list available models
        models.list()
        sys.exit(0)

    # load selected model, loads PerceptronWithMemoryModel by default
    try:
        logging.info('Loading model %s' % (opts.model_str))
        model = models.load(opts.model_str)
    except Exception as e:
        logging.error('Could not load model %s, with error %s' % (opts.model_str, str(e)))
        sys.exit(-1)

    # set screen width and height, create screen object and set fullscreen
    width = 1920
    height = 1080
    screen = pygame.display.set_mode((width, height), pygame.DOUBLEBUF)
    if not opts.windowed: pygame.display.toggle_fullscreen()

    if opts.scores:
        # if we want to just display the scores
        highscores = utils.HighScoresScreen(screen, 'scores.csv', player=player)
        highscores.main_loop()
    elif opts.gameonly:
        # run the game directly without start screens and detail entry etc...
        if opts.gameonly_model != '':
            model2 = models.load(opts.gameonly_model)
        else:
            model2 = None
        MainWindow = utils.GameScreen(screen, utils.ModelWrapper(model, prefix='plots/%s' % opts.id),
                                      test_mode=opts.test_mode,
                                      model2=model2, level=opts.gameonly_level)
        print('Score: %s ' % str(MainWindow.main_loop()))
    else:
        # if no other mode specified, run the full game
        MainWindow = utils.PyManMain(screen, utils.ModelWrapper(model, prefix='plots/%s' % opts.id),
                                    test_mode=opts.test_mode, no_sound=opts.no_sound)
        MainWindow.main_loop()
