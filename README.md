

# Click Predict Game README

A simple game to test different machine learning models against human opponents and against other models. 
The game is written in python and makes use of the pygame library for graphics and the standard numerical
python libraries including numpy, scipy, sklearn, matplotlib and seaborn.

## Getting Started

These instructions will get a copy of the project up and running. 

### Prerequisites

The game has been designed for use with python3 and requires the following pacakges.

```
matplotlib==2.0.2
numpy==1.13.1
pygame==1.9.3
pandas==0.20.3
scikit-learn==0.19.0
seaborn==0.8
```

### Installing

To set up a new environment with these packages these steps. First install vitualenv. On a debian based
system this can be achevied by install pip first and then virtualenv with the following commands.

```
sudo apt-get install python3-pip
sudo pip3 install virtualenv
```

Once this is done, create a virtual environment and activate it

```
virtualenv -p python3 preclict
source preclict/bin/activate
```

Then to install the game, from the source folder run the installer. This should install the required dependencies.

```
python setup.py install
```

### Running

To start the game, make sure the virtualenv is activated and then call the run_preclict.py script with 

```
run_preclict.py
```

This will start the game with an untrained single perceptron model. The aim of the game is to outsmart the model
and click the mouse button it is not expecting. In turn the model attempts to learn from the buttons and predict 
the button you will click next. When you get tired of playing exit the game and you will find a plot which shows 
how the game proceeded.

To just run the game without the score board or any other parts one can run the in gameonly mode with:
```
run_gameonly.py --gameonly
```

Additional arguments are available to control the behavior of the game. These are:

```
usage: run_preclict.py [-h] [-l] [-id ID] [-t] [-w] [--no-sound] [--scores]
                       [--gameonly] [--gameonly-level GAMEONLY_LEVEL]
                       [--gameonly-model GAMEONLY_MODEL]
                       [--log-level LOG_LEVEL]
                       [model_str]

positional arguments:
  model_str             Model to use with args appended after a full stop.

optional arguments:
  -h, --help            show this help message and exit
  -l, --list            List the models that one can use
  -id ID                Identifier to use for output files (default=hist).
  -t, --test-mode       Run in test mode without asking for user details.
  -w, --windowed        Start windowed
  --no-sound
  --scores
  --gameonly
  --gameonly-level GAMEONLY_LEVEL
  --gameonly-model GAMEONLY_MODEL
  --log-level LOG_LEVEL

```

## More advanced usage

### Running with other models

The models are stored in the models folder and new models which follow the correct format are automatically 
detected. See section about writing your own models below. To list the available models call the run script 
with the `-l` flag as 

```
run_preclict.py -l
```

To select a particular model simply pass that name to the script. It is possible to specify parameters using json
formatted string appended to the model name after a full stop. For example to instead load the multi input 
perceptron model with a memory of 10 enter

```
run_preclict.py PerceptronWithMemoryModel.{\"memory\":10}
```

Note that it is necessary to escape the double quotes. 

### Automated testing of models

There is a `run_preclict_tests.py` script which runs a number of standard tests for specified model(s). For 
example it tests  constant input, alternating, random, one model verses another and some simple patterns. 
To run these tests enter

```
run_preclict_tests.py
```

After running the results will appear as images in the plots folder.

Running with the `--help` flag will provide instructions on how one can specify the use of a different model and 
run a subset of the tests. 

### Writing your own models

The game has been designed so that it is easy to write your own models and play against them or evaluate them
against other models. The complete code for the perceptron model reads

```
import numpy as np

class PerceptronModel:
    """
    Simple one input, one output perceptron
    """
    @staticmethod
    def usage():
        return '\tPerceptronModel: \t\tSingle input, single output perceptron model'

    def __init__(self):
        self.ws = np.random.randn(2) # one bias and one weight
        self.x_old = 0 # initially set the former value to null value
        self.id = 'p1'

    def update_model(self, y):
        exp_y = self._predict(self.x_old)
        self.ws += (y-exp_y)*np.array([1.0, self.x_old])
        self.x_old = y

    def predict(self):
        return self._predict(self.x_old)

    def _predict(self, x):
        return (1 if (self.ws[0] + self.ws[1]*x) > 0 else -1)

    def weights(self):
        return self.ws.copy()

    def last_x(self):
        return self.x_old
```

All that is required of a model is that it is a class with a name ending in `Model` that implements all the public
methods that are present in the PerceptronModel class (all that don't begin with `_`).
